
# Reliably Demo Repository

This is a demo repository for Reliably, where Reliably will obtain any Reliability Advice and Reliability Suggestions based on the
`yaml` in the `manifests` folder in the repository.
Reliably is integrated by using GitLab CI/CD and the integration is fully documented in
the [Reliably Documentation][reliably-gitlab].

[reliably-gitlab]: https://docs.reliably.com/start/gitlab/#add-gitlab-cicd-to-your-repository

As a pre-requesite, the `RELIABLY_TOKEN` must be defined as a masked variable in your project's settings; [see doc](https://docs.gitlab.com/ee/ci/variables/#create-a-custom-variable-in-the-ui)

To integrate Reliably with a repository the `.gitlab-ci.yaml` should be setup as:

```yaml
variables:
  SOURCE_CODE: "manifests"

stages:
  - test

include:
  - template: Code-Quality.gitlab-ci.yml

code_quality:
  image:
    name: ghcr.io/reliablyhq/cli/cli:latest
    entrypoint: ["/bin/sh", "-c"]

  script:
    - reliably scan kubernetes $SOURCE_CODE --format codeclimate --output gl-code-quality-report.json || true
  stage: test
  allow_failure: true
  artifacts:
    when: always
    expose_as: 'Code Quality Report'
    paths: [gl-code-quality-report.json]
  rules:
    - if: $CI_COMMIT_BRANCH && $RELIABLY_TOKEN
      changes:
        - "manifests/*.{yaml}"
```

The Gitlab CI/CD job will run when a `.yaml` file is updated in the `manifests` folder.

Any Reliability Advice and Reliability Suggestions will be exposed in the `gl-code-quality-report.json` artifact, which can be downloaded.

The Advice can also be viewed in the GitLab UI as part of the Merge request or as part of the GitLab CI/CD Pipeline view.
